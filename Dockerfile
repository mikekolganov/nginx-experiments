FROM nginx
RUN apt-get update && apt-get install -y nano less nodejs
COPY ./server.crt /etc/nginx/ssl/nginx_mc.crt
COPY ./server.key /etc/nginx/ssl/nginx_mc.key
COPY ./api /opt/api
COPY ./app /opt/app
COPY ./static /opt/static
